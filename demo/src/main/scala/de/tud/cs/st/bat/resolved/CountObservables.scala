/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved

import analyses.{ Analysis, AnalysisExecutor, BasicReport, Project }
import java.net.URL
import instructions._
import scala.xml._
import org.jopendocument.dom.spreadsheet.SpreadSheet
import javax.swing.table.DefaultTableModel
import java.io.File
import scala.Array._

/**
 * checks occurances of observer related code
 *
 * @author Linus Armakola
 * @author Michael Eichberg
 */
object CountObservables extends AnalysisExecutor {
  object BasicClassTypeSignature {
    def unapply(cts: ClassTypeSignature): Option[ObjectType] = { Some(cts.objectType) }
  }
  object GenericContainer { // matches : List<Object>
    def unapply(cts: ClassTypeSignature): Option[(ObjectType, ObjectType)] = {
      cts match {
        case ClassTypeSignature(Some(cpn), SimpleClassTypeSignature(csn, Some(List(ProperTypeArgument(None, BasicClassTypeSignature(tp))))), List()) => Some((ObjectType(cpn + csn), tp))
        case _ => None
      }
    }
  }

  override def analysisParametersDescription: String =
    "-class=<The class for which the transitive closure of used classes is determined>"

  override def checkAnalysisSpecificParameters(parameters: Seq[String]): Boolean =
    parameters.size == 1 && parameters.head.startsWith("-class=")

  val analysis = new Analysis[URL, BasicReport] {

    def description: String = "Gives some details on how Observer Pattern is used in a project."

    def analyze(project: Project[URL], parameters: Seq[String]) = {
      val subject = parameters.head.substring(7).replace('.', '/')
      val totalClassfiles = project.classFiles.filter(_.thisType.fqn.startsWith(subject))
      val totalMethods = for {
        classFile <- totalClassfiles
        method <- classFile.methods
      } yield { method }

      val totalFields = for {
        classFile <- totalClassfiles
        field <- classFile.fields
      } yield { field }
      val totalStats = Vector[String](("Total Classfiles: " + totalClassfiles.size.toString), ("Total Methods: " + totalMethods.size.toString), ("Total Fields: " + totalFields.size.toString))
      //      find all Observerpattern relatad classnames
      val javaObservables = project.classHierarchy.allSubtypes(ObjectType("java/util/Observable"), false).toSet
      val javaObservers = project.classHierarchy.allSubtypes(ObjectType("java/util/Observer"), false).toSet
      val javaEventObjects = project.classHierarchy.allSubtypes(ObjectType("java/util/EventObject"), false).toSet
      val javaEventListeners = project.classHierarchy.allSubtypes(ObjectType("java/util/EventListener"), false).toSet
      val observers = project.classFiles.filter(_.thisType.fqn.endsWith("Observer")).map(_.thisType)
      val allObservers = observers.flatMap(project.classHierarchy.allSubtypes(_, false)).toSet
      val listeners = project.classFiles.filter(_.thisType.fqn.endsWith("Listener")).map(_.thisType)
      val allListeners = listeners.flatMap(project.classHierarchy.allSubtypes(_, false)).toSet
      val allObserverObjectTypes = (javaObservables ++ javaObservers ++ javaEventObjects ++ javaEventListeners ++ allObservers ++ allListeners).toSet
      //      Analisis for allObserverObjectTypes
      analyzeSubject(allObserverObjectTypes, project, subject, totalStats, "allObserverObjectTypes")
      //      Analisis for javaObservables
      analyzeSubject(javaObservables, project, subject, totalStats, "javaObservables")
      //      Analisis for javaObservers
      analyzeSubject(javaObservers, project, subject, totalStats, "javaObservers")
      //      Analisis for javaEventObjects
      analyzeSubject(javaEventObjects, project, subject, totalStats, "javaEventObjects")
      //      Analisis for javaEventListeners
      analyzeSubject(javaEventListeners, project, subject, totalStats, "javaEventListeners")
      //      Analisis for allObservers
      analyzeSubject(allObservers, project, subject, totalStats, "allObservers")
      //      Analisis for allListeners
      analyzeSubject(allListeners, project, subject, totalStats, "allListeners")
      // report
      BasicReport("Analysis completed")
    }
  }

  /**
   * The main Analysis which looks for fields that are of given Type
   */
  def analyzeSubject(objectTypes: Set[ObjectType], project: Project[URL], subject: String, stats: Vector[String], analysisName: String) {
    //      1. Collect all Fields that are of a type that is also in allObserverObjectTypes
    val objectTypeFields = collectFields(project, subject, objectTypes)
    //      2. Find all accesses on those fields and collect the accessed field with the current classfile and method
    val objectTypeFieldAccessesForOutput = (collectFieldaccesses(project, objectTypeFields.toSet, subject)).toSet
    val objectTypeClassFilesForOutput: Set[String] = for {
      access <- objectTypeFieldAccessesForOutput
    } yield {
      access._1.thisType.fqn.replace('/', '.')
    }
    val objectTypeMethodsForOutput: Set[String] = for {
      access <- objectTypeFieldAccessesForOutput
    } yield {
      (access._1.thisType.fqn + "{ " + access._2.toJava + " } -> " + access._3.name).replace('/', '.')
    }
    val objectTypeFieldsForOutput: Set[String] = for {
      field <- objectTypeFields
    } yield {
      field.name
    }
    //    3. Complete stats
    val allStats = stats ++ Vector[String](("Numbers concerning " + analysisName),
      ("Number of accesses on fields: " + objectTypeFieldAccessesForOutput.size.toString),
      ("Number of classfiles: " + objectTypeClassFilesForOutput.size.toString),
      ("Number of methods that access fields: " + objectTypeMethodsForOutput.size.toString),
      ("Number of fields that are accessed: " + objectTypeFieldsForOutput.size.toString))
    //     4. Write the data to an .ods file named (subject + "_" + analysisName + ".ods")
    val analysisList = Vector[Iterable[Any]](objectTypeClassFilesForOutput, objectTypeMethodsForOutput, objectTypeFieldsForOutput)
    analysisToOds(analysisList, subject, allStats, analysisName)
  }

  /**
   * Writes the collected data to a .ods file
   */
  def analysisToOds(analysisList: Vector[Iterable[Any]], subject: String, stats: Vector[String], analysisName: String) {
    val columns = Array[AnyRef]("Total Numbers", "Unique ClassFiles in which one of the methods is called or a field is declared", "Unique Methods calls that create/interact with Oberservers/Listener Fields", "Unique Fields which store Observers/Listener")
    var arrayColumnSize = 0
    if (analysisList(0).size + analysisList(1).size + analysisList(2).size < 20) { arrayColumnSize = 20 } else {
      arrayColumnSize = analysisList(0).size + analysisList(1).size + analysisList(2).size
    }
    val data = ofDim[AnyRef](arrayColumnSize, 6)
    var j = 0; var n = 0;
    stats.foreach(x => { data(n)(j) = x; n += 1 })
    analysisList.foreach(x => { var n = 0; j += 1; x.foreach(i => { data(n)(j) = i.toString; n += 1 }) })
    val model = new DefaultTableModel(data, columns)
    val fileName = (analysisName + ".ods")
    val file = new File(fileName)
    val spreadsheet = SpreadSheet.createEmpty(model)
    spreadsheet.saveAs(file)
  }

  /**
   * Collects all fields that are of a ObjectType which is also contained in the given Set[ObjectType]
   */
  def collectFields(project: Project[URL], subject: String, objectTypes: Set[ObjectType]): Set[Field] = {
    var subjectLength = subject.length()
    val javaCollections = project.classHierarchy.allSubtypes(ObjectType("java/util/Collections"), false).toSet
    val result = for {
      classFile <- project.classFiles
      if classFile.thisType.fqn.startsWith(subject)
      field <- classFile.fields
      thisFieldType = field.fieldType
      // if field if of ObjectType or is an array[ObjectType] check wether it is some kind of Listener
      if ((thisFieldType.isObjectType && objectTypes.contains(thisFieldType.asObjectType))
        || (thisFieldType.isArrayType && thisFieldType.asArrayType.elementType.isObjectType && objectTypes.contains(thisFieldType.asArrayType.elementType.asObjectType)))
    } yield {
      field
    }
    (result ++ collectLists(project, subject, objectTypes)).toSet
  }

  /**
   * Collects all Lists of fields that are of a ObjectType which is also contained in the given Set[ObjectType]
   */
  def collectLists(project: Project[URL], subject: String, objectTypes: Set[ObjectType]): Iterable[Field] = {
    var subjectLength = subject.length()
    val javaCollections = project.classHierarchy.allSubtypes(ObjectType("java/util/Collections"), false).toSet
    for {
      classFile <- project.classFiles
      if classFile.thisType.fqn.startsWith(subject)
      field <- classFile.fields
      thisFieldType = field.fieldType
      if (field.fieldTypeSignature.isDefined)
      Some(cts) = field.fieldTypeSignature
      // if field if of ObjectType or is an array[ObjectType] check wether it is some kind of Listener
      if ((thisFieldType.isObjectType && objectTypes.contains(thisFieldType.asObjectType))
        || (thisFieldType.isArrayType && thisFieldType.asArrayType.elementType.isObjectType && objectTypes.contains(thisFieldType.asArrayType.elementType.asObjectType))
        || (cts match {
          case GenericContainer(c, t) => true
          case _ => false
        }))
    } yield {
      field
    }
  }
  /**
   * collects all accesses on fields that are contained in the given Set[Fields]
   */
  def collectFieldaccesses(project: Project[URL], fields: Set[Field], subject: String): Set[(ClassFile,Method,Field)] = {
    var subjectLength = subject.length()
    val result = for {
      classFile <- project.classFiles
      if classFile.thisType.fqn.startsWith(subject)
      method @ MethodWithBody(code) <- classFile.methods
      if method.name != "<init>" && method.name != "<clinit>"
      (_, fa) <- code.collect({ case fa @ FieldWriteAccess((declClass, name, _)) if fields.exists(f => f.name == name && declClass == project.classFile(f).thisType) => fa })
    } yield {
    	val field=fields.find(f => f.name == fa.name && fa.declaringClass == project.classFile(f).thisType).get
      (classFile, method, field)
    }
    result.toSet
  }
}

class ReturnValue(classFiles: ClassFile, fields: Field, methods: Method) {
  val classFile = classFiles
  val field = fields
  val method = methods
}
