# Documentation for CountObservables

## Methods

### main method

The main method collects total numbers for the subject of the analysis and prepares sets of object types for the analyzeSubject Method. Those sets contain different Object Types connected to the Observer pattern. The Sets are:
+ javaObservables:			a set of all object types that are subtypes of java.util.Observable in the class hierarchy
+ javaObservers:			a set of all object types that are subtypes of java.util.Observer in the class hierarchy
+ javaEventObjects:			a set of all object types that are subtypes of java.util.EventObject in the class hierarchy
+ javaEventListeners:		a set of all object types that are subtypes of java.util.EventListener in the class hierarchy
+ allObservers:				a set of all object types that end with "Observer" and their subtypes
+ allListeners:				a set of all object types that end with "Listener" and their subtypes
+ allObserverObjectTypes:	a set of all object types that are contained in the sets above

The main method also prepares some fields for the output function analysisToOds:
+ totalClassfiles:	a iterable that holds the total number of all class files in the subject 
+ totalMethods:		a iterable that holds the total number of all methods in the subject 
+ totalFields:		a iterable that holds the total number of all fields in the subject 
+ subject:			a string given by the user which contains the package name of the subject that is analyzed
+ project:			the project the analysis is called with
+ totalStats:		a vector that holds totalClassfiles, totalMethods and totalFields

After those preparations the main method calls analyzeSubject for each set of object types

### analyzeSubject method

This method consumes a set of object types, project, subject, totalStats and a string to name the output file.
The method collects data concerning the given set of object types and calls analysisToOds to write the data to an .ods file.
The collected data is stored in those fields:
+ objectTypeFields:				a set of fields that contains the result of the call of collectFields with the current set of object types
+ objectTypeFieldAccessesForOutput:	a set of ReturnValues that holds all accesses on the objectTypeFields 
+ objectTypeClassFilesForOutput:	a set of strings containing all class file names of class files that either hold a observer field or have a method that accesses a  observer field 
+ objectTypeMethodsForOutput:		a set of strings containing all methods, the class file they are called in and the field they access
+ objectTypeFieldsForOutput:		a set of strings containing all fields

### collectFields and collectLists methods

These methods collect all fields in the subject that are of a object type or are a List of object types that is contained in the given set of object types.

### analysisToOds method

This method writes the consumed data to a .ods file using the jopendocument library.
The output file is described later.

## Output

The output file is produced by the analysisToOds method and is structured like this:
The first column contains total numbers of class files, methods and fields in the subject first and then numbers of accesses on fields, class files, methods that access fields and fields concerning the current object type.

The second column contains all entries of the set of class files concerning the current set of object types. That is every class file that either holds a field of a object type that is in the current set of object types or a method that accesses those fields. This also includes class files that inherit from a class file that holds such a field or has methods that accesses them.

The third column contains all entries of the set of methods that access fields that are listed in the fourth column. This also includes inherited methods. This can cause the analysis to produce a number of methods greater than the total number of methods in the project.

The fourth column contains all entries of the set of fields that are of object types that is in the current set of object types or are a list of those object types. The individual elements contained in a list are not listed.